<?php
 namespace App\Controller;

 use Symfony\Component\Routing\Annotation\Route;
 use Symfony\Component\HttpFoundation\Response;


class PageController
{
     /**
      * @Route("/")
      */
    public function page()
    {
        $link = getenv('SPOT_LINK');
            return new Response(
            '<html><body>'.$link.'</body></html>'
        );
    }
}