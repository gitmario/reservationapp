<?php

namespace App\Controller;

use App\Entity\Reservations;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Doctrine\DBAL\Connection;

class ResultsController extends AbstractController
{

    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @Route("/results", name="results")
     */
    public function index()
    {

        $results = $this->getDoctrine()->getRepository(Reservations::class)->findAll();

        
        return $this->render('results/results.html.twig', [
            'results' => $results,
        ]);
    }

    /**
     * @Route("/export", name="export")
     */


    public function csv()
    {
        $response = new StreamedResponse();
        $response->setCallback(function() {
            $handle = fopen('php://output', 'w+');


            fputcsv($handle, array('Full Name', 'Last Name', 'Email', 'Phone Number', 'Number of Guests', 'Date','Time', 'Reservation Type','Other', 'Any Special Requests'),';');

            $results = $this->connection->query("SELECT * from reservations");

            while($row = $results->fetch()) {
                fputcsv(
                    $handle,
                    array($row['fullname'], $row['lastname'], $row['email'], $row['phonenumber'], $row['number_of_guests'], $row['date'], $row['time'], $row['reservation_type'], $row['other'], $row['any_special_requests']),
                    ';'
                );
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;
    }
}