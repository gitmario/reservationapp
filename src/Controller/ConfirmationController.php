<?php
 namespace App\Controller;

 use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\Routing\Annotation\Route;
 use Symfony\Component\HttpFoundation\Response;


class ConfirmationController extends AbstractController
{
     /**
      * @Route("/confirmation", name="confirmation")
      */
    public function confirmation()
    {
        return $this->render('confirmation/confirmation.html.twig');
    }
}