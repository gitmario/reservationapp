<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Reservations;
use App\Form\ReservationsType;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/", name="registration")
     */
    public function index(Request $request)
    {
        $reservation = new Reservations();
        $form = $this->createForm( ReservationsType::class, $reservation);


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($reservation);
            $entityManager->flush();
            $this->addFlash(
                'success',
                'Your reservation has been successfully created!'
            );


            $to = "mario.trupkovic@gmail.com";
            $subject = 'subject';
            $message = 'form submission';
            $headers = array(
              'Form' => 'info@reservation.com',
              'Reply-To'=>'info@reservatoion.com',
              'X-Mailer'=> 'PHP/' .phpversion()
            );
            mail($to, $subject, $message, implode("\r\n", $headers));

            return $this->redirectToRoute('confirmation');
        }



        return $this->render('registration/index.html.twig', [
            'form' => $form->createView(),
        ]);


    }
}
