<?php

namespace App\Form;

use App\Entity\Reservations;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TelType;

class ReservationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', TextType::class, [
        'label'=> 'Full name',

        ])
            ->add('lastname', TextType::class, [
                'label'=> 'Last name',

            ])
            ->add('Email', EmailType::class)
            ->add('phonenumber', TelType::class, [
                'label'=>'Phone number'
            ])
            ->add('number_of_guests')
            ->add('date',DateType::class, [
        'widget' => 'single_text'
    ])
            ->add('time')
            ->add('reservation_type',ChoiceType::class, [
                'choices' => [
                    'pre-payment' => 'pre_payment',
                    'credit card' => 'credit_card',
                    'travel agents'=> 'travel_agents',
                    'other'=> 'other',
                ],
            ])
            ->add('other',TextareaType::class, [
                'required'=> false,
                'empty_data'=>'-',
                'attr' => [
                    'placeholder' => 'Please tell us more..'
                ],
            ])
            ->add('any_special_requests',TextareaType::class, [
                'required'=> false,
                'empty_data'=>'-',
                'attr' => [
                    'placeholder' => 'Your message...'
                ],
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservations::class,
        ]);
    }
}
